/**
 *
 * @author Daniel Ormeño - s2850944
 *         Griffith University, Gold Coast Campus
 *         7004ICT Data Communication
 *         Assignment 2 Proxy Server
 */
public class CacheInstance { 
    /* Instance variables*/
    final static int MAX_OBJECT_SIZE = 100000;
    private  String url;
    /* Instance variables for cached response*/
    private String statusLine;
    private String headers="";
    private String crlf;
    byte[] body = new byte[MAX_OBJECT_SIZE] ;
    
    /* Constructors */        
    public CacheInstance (String url, String statusLine, String headers, String crlf, byte[] body){
        this.url=url;
        this.statusLine=statusLine;
        this.headers=headers;
        this.crlf=crlf;
        this.body=body;
    }
    
    /* Accessors*/
    public String getURL(){
        return this.url;
    }
    
    public String getStatusLine(){
        return this.statusLine;
    }
    
    public String getHeaders(){
        return this.headers;
    }
    
    public String getCRLF(){
        return this.crlf;
    }
    
    public byte[] getBody(){
        return this.body;
    }
    
    /* Mutators*/
    
    public void setURL(String url){
        this.url=url;
    }
    
    /* methods*/
    public String toString(){
        return url+statusLine+headers+crlf+body;
    }
    
}

