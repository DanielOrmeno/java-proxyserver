
/**
 *
 * @author Daniel Ormeño - s2850944 Griffith University, Gold Coast Campus
 * 7004ICT Data Communication Assignment 2 Proxy Server
 */
import java.net.*;
import java.io.*;
import java.util.*;
import javax.swing.JTextArea;

public class ProxyCache {

    /**
     * Port for the proxy
     */
    private static int port;
    /**
     * Socket for client connections
     */
    private static ServerSocket socket;
    /**
     * Variable urlRequested for checking the cache
     */
    private static String urlRequested = "";
    /**
     * Array of HttpRequest objects
     */
    private static CacheInstance[] cacheArray = new CacheInstance[500];
    /**
     * Index used for adding new objects to the cacheArray
     */
    private static int j = 0;

    /**
     * Create the ProxyCache object and the socket
     */
    public static void init(int p) {
        port = p;
        try {
            /*
             * Creates a new socket with the value of the variable "port" as
             * port number.
             */
            socket = new ServerSocket(port);
        } catch (IOException e) {
            System.out.println("Error creating socket: " + e);
            System.exit(-1);
        }
    }

    public static void handle(Socket client) {
        Socket server = null;
        HttpRequest request = null;
        HttpResponse response = null;
        // Flag for found URL in cache
        Boolean urlFound = false;

        /**
         * Index of found entry
         */
        
        int indexOfEntry = 501; //Index Out of bounds

        /*
         * Process request. If there are any exceptions, then simply return and
         * end this request. This unfortunately means the client will hang for 
         * a while, until it timeouts.
         */

        /*
         * Read request
         */
        try {
            /*
             * Creates a new Buffered reader"fromClient" to process the http
             * parameters. This buffered reader (fromClient) is used as the
             * parameter of the newly created http request.
             */
            BufferedReader fromClient = new BufferedReader(new InputStreamReader(client.getInputStream()));
            request = new HttpRequest(fromClient);
        } catch (IOException e) {
            System.out.println("Error reading request from client: " + e);
            return;

        }

        /*
         * Checks if request exists in cache.
         */
        urlRequested = request.getURL(); // Stores requested URL

        /*
         * Check cacheArray for the requested URL
         */
        for (int i = 0; i < j; i++) {

            /*
             * Compares URL of each entry to the cache to requestedURL
             */
            if (cacheArray[i].getURL().equals(urlRequested)) {
                indexOfEntry = i;
                System.out.println("URL found in cache, entry No:" + indexOfEntry);
                System.out.println("*------------------------------------------*");
                // urlFound set to true if requestedURL is found in cache
                urlFound = true;
                break;
            } else {
                urlFound = false;
            }
        }


        /*
         * Sends request to server if the url is not found
         */
        if (urlFound == false) {
            try {
                System.out.println("URL not found in cache, contacting server");
                /*
                 * Open socket and write request to socket
                 */
                server = new Socket(InetAddress.getByName(request.getHost()), request.getPort());
                DataOutputStream toServer = new DataOutputStream(server.getOutputStream());
                toServer.writeBytes(request.toString());
            } catch (UnknownHostException e) {
                System.out.println("Unknown host: " + request.getHost());
                System.out.println(e);
                return;
            } catch (IOException e) {
                System.out.println("Error writing request to server: " + e);
                return;
            }
            /*
             * Read response and forward it to client
             */
            try {
                DataInputStream fromServer = new DataInputStream(server.getInputStream());
                response = new HttpResponse(fromServer);
                DataOutputStream toClient = new DataOutputStream(client.getOutputStream());
                /*
                 * Write response to client. First headers, then body
                 */
                toClient.writeBytes(response.statusLine);
                /** status line to be cached*/
                String tempStatusLine = response.statusLine; 
                toClient.writeBytes(response.headers);
                /** headers to be cached*/
                String tempHeaders = response.headers; 
                toClient.writeBytes(HttpResponse.CRLF);
                /** CRLF to be cached*/
                String tempCRLF = HttpResponse.CRLF; 
                toClient.write(response.body);
                /** body to be cached*/
                byte[] tempBody = response.body; 
                client.close();
                server.close();

                /*
                 * Writes response payload and url to cache (Checks if there is
                 * free space in the cache)
                 */
                if (j <= 500) {
                    cacheArray[j] = new CacheInstance(urlRequested, tempStatusLine, tempHeaders, tempCRLF, tempBody);
                    System.out.println(urlRequested + " Saved to cache, entry No: " + j);
                    j++; // Incremented so cache entries are not overwrited
                    System.out.println("//////////////////////////////////////");
                } else {
                    System.out.println("Cant save to cache, memory full");
                }
                /*
                 * ----------------------------------------
                 */
            } catch (IOException e) {
                System.out.println("Error writing response to client: " + e);
            }
        } else {
            try {

                System.out.println("Loading data from cache");

                /*
                 * Write response to client from cache.
                 */
                DataOutputStream toClient = new DataOutputStream(client.getOutputStream());
                /** Variable indexOfEntry sets the index of the array that 
                 * references the object containing found entry in cache */
                toClient.writeBytes(cacheArray[indexOfEntry].getStatusLine());
                toClient.writeBytes(cacheArray[indexOfEntry].getHeaders());
                toClient.writeBytes(cacheArray[indexOfEntry].getCRLF());
                toClient.write(cacheArray[indexOfEntry].getBody());
                client.close();
                System.out.println("Data successfully retrieved from cache, entry No" + indexOfEntry);
                System.out.println("//////////////////////////////////////");
            } catch (IOException e) {
                System.out.println("Error writing response to client: " + e);
            }
        }
    }

    /*
     * --------------------------------------------------
     */
    /**
     * Read command line arguments and start proxy
     */
    public static void main(String args[]) {
        int myPort = 0;
        /**
         * Initiates Interface
         */
        new ProxyCacheUI().setVisible(true);
        try {
            myPort = Integer.parseInt(args[0]);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Need port number as argument");
            System.exit(-1);
        } catch (NumberFormatException e) {
            System.out.println("Please give port number as integer.");
            System.exit(-1);
        }

        init(myPort);

        /**
         * Main loop. Listen for incoming connections and spawn a new thread for
         * handling them
         */
        Socket client = null;

        while (true) {
            try {
                client = socket.accept();
                handle(client);
            } catch (IOException e) {
                System.out.println("Error reading request from client: " + e);
                /*
                 * Definitely cannot continue processing this request, so skip
                 * to next iteration of while loop.
                 */
                continue;
            }
        }

    }
}
 
